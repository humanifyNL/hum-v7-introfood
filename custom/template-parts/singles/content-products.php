<?php
/**
 * The default template for displaying post content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  get_template_part( 'template-parts/singles/products/header', 'products' );
  ?>

  <div class="post-content">

    <?php
    if ( get_post_type( get_the_ID() ) == 'products' ) {
      get_template_part( 'template-parts/site/share-dialog');
    }

    if ( have_rows( 'flex_content_post' ) ) {
      get_template_part( 'template-parts/acf/flex-product' );
    } else {
      get_template_part( 'template-parts/site/the-content');
    }

    if ( get_post_type( get_the_ID() ) == 'products' ) {
      get_template_part( 'template-parts/site/share-dialog-bot');
    }

    get_template_part( 'template-parts/singles/post/meta', 'post__bot' );
    ?>

	</div>

  <?php
  get_template_part('template-parts/acf/queries/row--products__rel' );
  ?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php

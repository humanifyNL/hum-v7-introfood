<?php
/**
 * Query posts - related
 *
 * @package hum-v7-core
 */

$n_posts = get_field( 'related_posts_amount', 'option');

if ( $n_posts != 0 ) {

  $current_postid = get_the_id();

  $cat_ids = hum_term_ids('category');
  $tag_ids = hum_term_ids('post_tag');

  $args_rp = array(
    'posts_per_page' => $n_posts,
    'post_type' => array( 'products' ),
    'post__not_in' => array( $current_postid ),
    'order' => 'DESC',
    /*
    'tax_query' => array(
      'relation' => 'OR',
      array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => $cat_ids,
        'include_children' => true,
        'operator' => 'IN'
      ),
      array(
        'taxonomy' => 'post_tag',
        'field' => 'id',
        'terms' => $tag_ids,
        'operator' => 'IN'
      )
    ),
    */
  );

  $query_posts = new WP_Query( $args_rp );

  if ( $query_posts->have_posts() ) {

    ?>
    <div class="grid--previews <?php echo hum_grid_preview(); ?> related">

      <?php

      while ( $query_posts -> have_posts() ) {

        $query_posts -> the_post();
        include( locate_template( 'template-parts/singles/post/preview-post.php' ) );

      }

      wp_reset_postdata();
      ?>

    </div>
    <?php
  }
}

<?php
/**
 * Text section
 *
 * ACF field: group_5f08831a932a6
 *
 * @package hum-v7-core
 */
?>

<div class="block block--meta">

  <?php
  include( locate_template( 'template-parts/acf/partials/text.php') );
  include( locate_template( 'template-parts/acf/partials/link__repeater-ext.php') );
  ?>

</div>

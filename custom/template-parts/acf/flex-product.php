<?php
/**
 * Flex content - post
 *
 * ACF field: group_5f1451d459925
 *
 * @package hum-v7-core
 */

if ( have_rows( 'flex_content_product' )) {

  while ( have_rows( 'flex_content_product' )) {

    the_row();

    // layouts
    if ( get_row_layout() == 'text' ) {

      get_template_part( 'template-parts/acf/rows/row--text-wysi' );
    }

    if ( get_row_layout() == 'image' ) {

      get_template_part( 'template-parts/acf/rows/row--image' );
    }

    if ( get_row_layout() == 'collapse' ) {

      get_template_part( 'template-parts/acf/rows/row--collapse' );
    }

    if ( get_row_layout() == 'image_slider' ) {

      get_template_part( 'template-parts/acf/rows/row--slider' );
    }

    if ( get_row_layout() == 'image_gallery' ) {

      get_template_part( 'template-parts/acf/rows/row--gallery' );
    }

    if ( get_row_layout() == 'form' ) {

      get_template_part( 'template-parts/acf/rows/row--form' );
    }

    if ( get_row_layout() == 'link_posts' ) {

      get_template_part( 'template-parts/acf/rows/row--postlinks' );
    }

  }

}

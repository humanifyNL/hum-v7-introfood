<?php
/**
  * Tab collapse row
  *
  * @package hum-v7-core
  */
?>

<section class="row row--tab <?php echo hum_row_style(); ?>" <?php hum_row_img(); ?>>

  <div class="section-body">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <div class="block block--tabs">

        <?php
        include( locate_template( 'template-parts/acf/partials/tab.php') );
        ?>

      </div>

    </div>

  </div>

</section>

<?php
/**
 * Hum Base font loading
 *
 * @package hum-v7-core
 */

if ( ! function_exists( 'hum_core_fonts_url' ) ) {

 /**
 * Return the Google fonts stylesheet URL if available.
 * The use of Open Sans by default is localized.
 *
 * @return string Google fonts URL for the theme or empty string if disabled.
 * @link http://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 */
 function hum_core_fonts_url() {

   $fonts_url = '';
   $fonts     = array();
   $subsets   = 'latin,latin-ext';

   /* translators: If there are characters in your language that are not supported by Open Sans, translate this to 'off'. Do not translate into your own language. */
   if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'hum-base' ) ) {
     $fonts[] = 'Open Sans:400italic,700italic,400,700';
   }

   if ( 'off' !== _x( 'on', 'Questrial font: on or off', 'hum-base' ) ) {
     $fonts[] = 'Questrial:300,400';
   }

   /* Support for aditional Google Fonts. Load Google Fonts stylesheet.
   *
   * We recommend using no more than 3 fonts styles.
   */


   if ( $fonts ) {
     $fonts_url = esc_url( add_query_arg( array(
     'family' => urlencode( implode( '|', $fonts ) ),
     ), 'https://fonts.googleapis.com/css' ) );
   }
   return $fonts_url;
  }

}


/**
 * Add preconnect for Google Fonts.
 *
 * @param array   $urls URLs to print for resource hints.
 * @param string  $relation_type The relation type the URLs are printed.
 * @return array URLs to print for resource hints.
 */
function hum_core_resource_hints( $urls, $relation_type ) {

 if ( wp_style_is( 'hum_fonts', 'queue' ) && 'preconnect' === $relation_type ) {

   // WordPress versions 4.7+ include a crossorigin attribute, earlier versions will not.
   $urls[] = array(
     'href' => 'https://fonts.gstatic.com',
     'crossorigin',
   );

 }

 return $urls;
}

add_filter( 'wp_resource_hints', 'hum_core_resource_hints', 10, 2 );

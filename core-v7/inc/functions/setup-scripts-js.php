<?php
/**
 * JS scripts for functionality
 *
 * @package hum-v7-core
 */

wp_enqueue_script( 'hum_clickable',
get_template_directory_uri() . '/assets/js/clickable.js',
array(),
'6.0.0',
true ); // Load in footer

wp_enqueue_script( 'hum_smoothscroll',
get_template_directory_uri() . '/assets/js/smoothscroll.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_has-link',
get_template_directory_uri() . '/assets/js/has-link.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_ext-link',
get_template_directory_uri() . '/assets/js/external-link.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_scroll_top',
get_template_directory_uri() . '/assets/js/scroll-top.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_scroll_bottom',
get_template_directory_uri() . '/assets/js/scroll-bottom.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_totop_button',
get_template_directory_uri() . '/assets/js/totop-button.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_viewport',
get_template_directory_uri() . '/assets/js/in-viewport.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_toggle_collapse',
get_template_directory_uri() . '/assets/js/toggle-collapse.js',
array(),
'6.0.0',
true );

wp_enqueue_script( 'hum_touchstart',
get_template_directory_uri() . '/assets/js/touchstart.js',
array(),
'6.0.0',
true );

/*
 * restricted to post type
 *
 */
if ( is_singular('post' ) ) {

  wp_enqueue_script( 'hum_share-dialog',
  get_template_directory_uri() . '/assets/js/share-dialog.js',
  array(),
  '6.0.0',
  true );

  wp_enqueue_script( 'hum_copy-clip',
  get_template_directory_uri() . '/assets/js/copy-to-clip.js',
  array(),
  '6.0.0',
  true ); // Load in footer
}


/*
 * external vendors
 *
 */
wp_enqueue_script( 'hum_isotope',
get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js',
array(),
'3.0.6',
true );

wp_enqueue_script( 'hum_isotope_init',
get_template_directory_uri() . '/assets/js/isotope-init.js',
array(),
'3.0.6',
true );

wp_enqueue_script( 'hum_base_swiper',
get_template_directory_uri() . '/assets/js/swiper.min.js',
array(),
'5.4.5',
true );

wp_enqueue_script( 'hum_base_swiper_init',
get_template_directory_uri() . '/assets/js/swiper-init.js',
array(),
'5.4.5',
true );

wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBpdCzkqTljPEYkH5kDBddAUgfAopFr3to', array(), '3', true );
wp_enqueue_script( 'google-map-init', get_template_directory_uri() . '/assets/js/googlemaps-acf.js', array('google-map', 'jquery'), '0.1', true );

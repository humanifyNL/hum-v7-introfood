<?php
/**
 * hum-base page-nav template functions
 *
 * @package hum-v7-core
 */

if ( ! function_exists( 'hum_page_nav' ) ) {
/**
 * Display navigation to next/previous post pages when applicable (when post is devided into multiple pages with <!--nextpage--> ).
 */

  function hum_page_nav() {

   wp_link_pages( array(
     'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Paginas:', 'hum-base' ) . '</span>',
     'after'       => '</div>',
     'link_before' => '<span>',
     'link_after'  => '</span>',
     'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Pagina', 'hum-base' ) . ' </span>%',
     'separator'   => '<span class="screen-reader-text">, </span>',
   ) );
   //return $content;
 }

 // add_filter ('the_content', 'hum_page_nav');
}


if ( ! function_exists( 'hum_archive_page_nav' ) ) {
  /**
  * Display navigation to next/previous archive pages when applicable
  */
 	function hum_archive_page_nav() {

 		the_posts_pagination( array(
 			'prev_text'          => esc_html__( 'Vorige', 'hum-base' ),
 			'next_text'          => esc_html__( 'Volgende', 'hum-base' ),
 			'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'hum-base' ) . ' </span>',
 		) );
 	}
}

<?php
/**
 * WIP - List style modifier for text-R partial
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_block_style' ) ) {

  function hum_block_style() {

    $block_style = get_sub_field( 'block_type' ,'option');
    $block_style = get_sub_field( 'block_type' );

    if ( isset($block_style) ) {

      if ( $block_style !== 'default' ) {

        $style = $block_style;

      } else {

        return;

      }

      return $style;
    }
  }
}




/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = block_type
 */

function acf_load_block_type_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'default' => 'Default',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=block_type', 'acf_load_block_type_field_choices');

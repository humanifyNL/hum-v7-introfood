<?php
/**
 * Utility functions
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_term_extract' ) ) {

  /**
   * create array with term_ids from an existing query
   * Uses $query (valid wp_query) and $taxonomy
   * returns array with term_ids
   */

  function hum_term_extract( $query, $taxonomy ) {

    // prep array for term_ids
    $queried_term_ids = array();
    // get list of post id's in query
    $queried_post_ids = wp_list_pluck( $query->posts, 'ID' );

    foreach ( $queried_post_ids as $post_id ) {
      // get terms for each post_id
      $queried_terms = wp_get_post_terms( $post_id, $taxonomy);
      // put term_id in array
      $queried_term_ids[] = $queried_terms[0]->term_id;
    }
    // filter duplicates out of array
    $queried_term_ids = array_unique($queried_term_ids, SORT_REGULAR);
    return $queried_term_ids;
  }
}

// grab term ids from a post
if ( !function_exists( 'hum_term_ids' ) ) {

  function hum_term_ids( $tax ) {

    if ( !isset( $tax ) ) {
      return;
    }

    global $post;

    $tax_terms = get_the_terms( $post, $tax );

    if ( !empty( $tax_terms ) ) {

      $tax_ids = wp_list_pluck( $tax_terms, 'term_id' );

      return $tax_ids;
    }

  }
}

// get all current taxonomy terms and output array
if ( !function_exists( 'hum_current_tax' ) ) {

  function hum_current_tax() {

    $args = array(
      'public'   => true,
    );
    $output = 'names'; // names or objects

    $all_tax = get_taxonomies( $args, $output );

    // remove post formats
    unset($all_tax['post_format'] );

    return $all_tax;
  }
}

if ( !function_exists( 'hum_term_names' ) ) {

  function hum_term_names( $tax ) {

    if ( !isset( $tax ) ) {
      return;
    }

    global $post;

    $tax_terms = get_the_terms( $post, $tax );

    if ( !empty( $tax_terms ) ) {

      $tax_names = wp_list_pluck( $tax_terms, 'name' );

      return $tax_names;
    }

  }
}

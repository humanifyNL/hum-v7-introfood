<?php
/**
 * Search results template
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<section class="row row--form">

				<div class="block-body wrap">

					<?php
					get_search_form(); ?>

				</div>

			</section class="row row--form">

			<?php
			if ( have_posts() ) {

				?>
				<section class="row--bar">
	        <div class="wrap">
	          <h1 class="page-title">
	            <?php
	            /* translators: %s: search query. */
	            printf( esc_html__( 'Zoekresultaten voor %s', 'hum-base' ), '<span>"' . get_search_query() . '"</span>' );
	            ?>
	          </h1>
	        </div>
				</section>

        <section class="row row--previews search">
        	<div class="wrap">
        		<div class="grid">

						<?php
		        while ( have_posts() ) {

		          the_post();
		          get_template_part( 'template-parts/pages/search/preview', 'search' );

		        }

		        hum_archive_page_nav();
						?>

        		</div>
					</div>
				</section>
				<?php

      } else {

				get_template_part( 'template-parts/pages/content', 'noresults' );

      }
			?>

		</main>

	</section>

</div>

<?php
get_footer();

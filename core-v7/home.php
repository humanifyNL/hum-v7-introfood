<?php
/**
 * The blog-index template
 *
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

  <div id="primary" class="content-area">

    <main id="main" class="site-main">

      <?php
      get_template_part( 'template-parts/pages/page/header', 'page__archive' );

      get_template_part( 'template-parts/acf/queries/row--posts__iso' );
      ?>

    </main>

  </div>

</div>

<?php
get_footer();

/**
 * Add link class
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  $("a").parent("li").addClass("has-link");

	$('.has-link').on('click', function () {
		window.location.href = $(this).find('a').attr('href');
	});

});

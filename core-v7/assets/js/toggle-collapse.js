/**
 * Add toggle class
 * to <span class="infocard"> when mouse click on <span class="info">
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  $(".collapse--wrap").click(function() {

    const shouldExpand = !$(this).hasClass("toggled");

    var contentHeight = $(this).find(".block__text").outerHeight();
    var heightPixels = contentHeight + "px";

    // 80 is minimum height set in CSS .block--collapser.scss
    if ( contentHeight < 80 ) {
      var height = 80;
    } else {
      var height = heightPixels;
    }

    $(".toggled").toggleClass("toggled");

    if (shouldExpand) {

      $(this).children(".collapse--card").toggleClass("toggled");
      $(this).toggleClass("toggled");
      // add height when expanded
      $(this).children(".collapse--card").css("height", height);
      // remove height from other cards that are open
      $(".collapse--wrap").not(this).children(".collapse--card").css("height", "0");

    } else {

      // remove height for cards that aren't clicked
      $(this).children(".collapse--card").css("height", "0");

    }

  });
});

<?php
/**
 * Query posts
 *
 * @package hum-v7-core
 */

$post_order = get_sub_field( 'post_query_order' );
$post_amount = get_sub_field( 'post_query_amount' );
$post_type = get_sub_field( 'post_query_type' );

$args = array(
  'post_type' => array( $post_type ),
  'orderby' => 'date',
  'order' => $post_order,
  'posts_per_page' => $post_amount,
);

$query_posts = new WP_query ( $args );

if ( $query_posts->have_posts() ) {

  ?>
  <div class="grid--previews <?php echo hum_grid_preview();?>">

  <?php
  while ( $query_posts->have_posts() ) {

    $query_posts->the_post();
    include( locate_template( 'template-parts/singles/post/preview-post.php' ) );

  }

  wp_reset_postdata();
  ?>

</div>
<?php

}

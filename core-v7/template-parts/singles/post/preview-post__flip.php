<?php
/**
 * Post preview with flip hover effect
 *
 * @package hum-v7-core
 */

?>
<article id="post-<?php the_id();?>" class="preview preview--post flip">

  <div class="flip-card">

    <div class="flip-card-inner">

      <div class="block__body flip--front">

        <?php
        // thumbnail
        $thumb = get_the_post_thumbnail( $post->ID,'medium' );

        echo '<div class="block__thumb">';
          if ( !$thumb ) { echo hum_default_img();
          } else { echo $thumb; }
        echo '</div>';
        }

        // title
        echo '<h3 class="block__title">'; the_title(); echo '</h3>';
        ?>

      </div>

      <div class="block__body flip--back">

        <?php
        // title
        echo '<h3 class="block__title">'; the_title(); echo '</h3>';

        // excerpt
        hum_excerpt( 'block__text' );

        echo '<div class="block__footer">';
          echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ). '">Lees artikel</a>';
        echo '</div>';
        ?>

      </div>


    </div>

  </div>

</article>

<?php
/**
 * Post preview part
 *
 * @package hum-v7-core
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview--post <?php if ( !empty($enable_iso) ) { hum_iso_class('category'); }?>">

  <?php
  // thumbnail
  $thumb = get_the_post_thumbnail( $post->ID,'medium' );

  echo '<div class="block__thumb">';
    if ( !$thumb ) { echo hum_default_img();
    } else { echo $thumb; }
  echo '</div>';
  ?>

  <div class="block__body">

    <?php
    echo '<h3 class="block__title">'; the_title(); echo '</h3>';

    hum_excerpt( 'block__text is-excerpt' );

    // meta
    echo '<div class="block__meta">';
      hum_meta_published();
      hum_meta_cp('category', 'button');
      hum_meta_cp('post_tag', 'button--alt');
    echo '</div>';

    // link
    $link_title = get_field( 'post_links_title' , 'option');

    echo '<div class="block__footer">';
      echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ). '">'.$link_title.'</a>';
    echo '</div>';
    ?>

  </div>

</article>

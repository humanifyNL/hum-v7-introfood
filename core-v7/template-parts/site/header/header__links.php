<?php
/**
 * Header links
 *
 * @package hum-v7-core
 */

$enable_links = get_field( 'enable_header_links', 'option' );
$header_links = get_field( 'contact_types', 'option' );

if ( $enable_links ) {

  ?>
  <div class='header__links'>

    <ul class="list--hor list--right list--icon">

      <?php
      $hd_email = get_field('contact_email', 'option');
      $hd_phone = get_field('contact_phone', 'option');
      $hd_phone_title = get_field('contact_phone_txt', 'option');

      if ( $header_links && in_array( 'phone', $header_links ) ) {
        echo '<li><a class="phone" href="tel:'.$hd_phone.'" rel="nofollow" target="_blank">'.$hd_phone_title.'</a></li>';
      }
      if ( $header_links && in_array( 'email', $header_links ) ) {
        echo '<li><a class="email" href="mailto:'.$hd_email.'" rel="nofollow" target="_blank">'.$hd_email.'</a></li>';
      }
      ?>

    </ul>
  </div>
  <?php
}
?>

<?php
/**
 * Hum Base buttons for sharing on social media
 *
 * https://css-tricks.com/how-to-use-the-web-share-api/
 * https://css-tricks.com/simple-social-sharing-links/
 *
 * @package hum-v7-core
 */

$share_text = 'Deel dit artikel';
$share_btn = 'Delen';
$share_close = 'Sluit';
$share_copy = 'Copy URL';
$share_url = get_permalink();
?>

<section class="row row--share row--share--bot">

  <div class="wrap">

    <div class="block block--share block--share--bot">

      <div class="button-group">

        <button class="btn button--g share-button bottom" type="button" title="Deel dit artikel">Delen</button>
        <button class="btn button--g dialog__close bottom"><?php echo $share_close; ?></button>

      </div>


      <div class="share-dialog dialog bottom">

        <div class="dialog__wrap">

          <div class="button-group button-group--dialog">

            <a class="btn button email share">E-mail</a>
            <a class="btn button facebook share">Facebook</a>
            <a class="btn button linkedin share">LinkedIn</a>
            <a class="btn button twitter share">Twitter</a>

          </div>

          <div class="link">

            <div class="dialog__link copy-link"><?php echo $share_url; ?></div>
            <button class="btn button--g dialog__btn copy-link"><?php echo $share_copy; ?></button>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>

<?php
/**
 * Query row - landing pages
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews landing <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      ?>

      <div class="block block--previews">

        <?php
        include( locate_template( 'template-parts/pages/landing/query-landing.php' ) );
        ?>

      </div>

    </div>

  </div>

</section>

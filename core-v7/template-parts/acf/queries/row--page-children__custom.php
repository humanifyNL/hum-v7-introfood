<?php
/**
 * Query row - page children - custom parents
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews row--previews--page page_children custom <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

	<div class="wrap">

		<div class="grid <?php echo hum_grid_section(); ?>">

			<div class="block block--previews">

				<?php
				include( locate_template( 'template-parts/pages/page/query-page-children__custom.php' ) );
				?>

			</div>

		</div>

	</div>

</section>

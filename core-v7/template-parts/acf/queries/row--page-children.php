<?php
/**
 * Query row - page children
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews row--previews--page page_children <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

	<div class="wrap">

		<div class="grid">

			<div class="block block--previews">

				<?php
				include( locate_template( 'template-parts/pages/page/query-page-children.php' ) );
				?>

			</div>

		</div>

	</div>

</section>

<?php
/**
 * Query row - posts - custom select
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews posts_select <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      ?>

      <div class="block block--previews">

        <?php
        include( locate_template( 'template-parts/singles/post/query-posts__select.php' ) );
        ?>

      </div>

    </div>

  </div>

</section>

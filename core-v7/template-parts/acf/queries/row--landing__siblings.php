<?php
/**
 * Query row - landing pages
 *
 * @package hum-v7-core
 */

$preview_type = get_field( 'landing_preview_type', 'option' ); // yes is icon, no is block
$landing_title = get_field( 'landing_siblings_title', 'option' );

?>
<section class="row row--previews landing_siblings <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

	<div class="wrap">

		<?php
		if ( $landing_title ) {
			echo '<h2 class="row__title">'.$landing_title.'</h2>';
		}

		include( locate_template( 'template-parts/pages/landing/query-landing__siblings.php' ) );
		?>

	</div>

</section>

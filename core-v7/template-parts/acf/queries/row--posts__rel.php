<?php
/**
 * Related posts query
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews posts_rel <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      $post_sib_title = get_field( 'related_posts_title', 'option');

      if ( $post_sib_title ) {
        ?>
        <div class="block block--text">
          <?php
          echo '<h2>'.$post_sib_title.'</h2>';
          ?>
        </div>
        <?php
      }
      ?>

      <div class="block block--previews">

        <?php
        include( locate_template( 'template-parts/singles/post/query-posts__rel.php' ) );
        ?>

      </div>

    </div>

  </div>

</section>

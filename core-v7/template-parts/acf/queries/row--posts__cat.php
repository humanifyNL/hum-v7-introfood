<?php
/**
 * Query row - posts
 *
 * @package hum-v7-core
 */

$main_id = get_the_id();
$cat_id = get_sub_field( 'page_category_id', $main_id);

if ( $cat_id ) {

  ?>
  <section class="row row--previews posts_cat <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

    <div class="wrap">

      <div class="grid <?php echo hum_grid_section(); ?>">

        <?php
        include( locate_template( 'template-parts/acf/blocks/block--text.php') );
        ?>

        <div class="block block--previews">

          <?php
          include( locate_template( 'template-parts/singles/post/query-posts__cat.php' ) );
          ?>

        </div>

      </div>

    </div>

  </section>
  <?php
}

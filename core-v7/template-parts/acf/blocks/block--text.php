<?php
/**
 * Block--text
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'text_wysi' ) ) {

  ?>
  <div class="block block--text <?php echo hum_block_style();?>">

    <?php
    include( locate_template( 'template-parts/acf/partials/text__wysi.php') );
    include( locate_template( 'template-parts/acf/partials/link__repeater.php') );
    ?>

  </div>
  <?php

}

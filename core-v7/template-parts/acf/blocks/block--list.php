<?php
/**
 * Block--list
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'list_repeater' ) ) {

  ?>
  <div class="block block--list <?php echo hum_block_style();?>">

    <?php
    include( locate_template( 'template-parts/acf/partials/title.php') );
    include( locate_template( 'template-parts/acf/partials/list__repeater.php') );
    include( locate_template( 'template-parts/acf/partials/link__repeater-r.php') );    ?>

  </div>
  <?php
  
}

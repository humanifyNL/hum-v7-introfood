<?php
/**
 * Block--gallery
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'gallery' ) ) {

  ?>
  <div class="block block--gallery">

    <?php
    include( locate_template( 'template-parts/acf/partials/gallery.php' ) );
    ?>

  </div>
  <?php

}

<?php
/**
 * Block--collapse
 *
 * @package hum-v7-core
 */

if ( have_rows( 'collapse_repeater' ) ) {

  ?>
  <div class="block block--collapse">

    <?php
    include( locate_template( 'template-parts/acf/partials/collapse.php' ) );
    ?>

  </div>
  <?php

}

<?php
/**
 * Block--form
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'form_form' ) ) {

  ?>
  <div class="block block--form">

      <?php
      include( locate_template( 'template-parts/acf/partials/form.php' ) );
      ?>

  </div>
  <?php

}

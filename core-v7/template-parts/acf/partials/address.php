<?php
/**
 * Address
 *
 * @package hum-v7-core
 */

$loc_title = get_field ( 'location_title', 'option' );

if ( have_rows( 'contact_location_rep', 'option' ) ) {

  while ( have_rows( 'contact_location_rep', 'option' ) ) {

    the_row();
    // vars
    $loc_name = get_sub_field ( 'location_name', 'option' );
    $loc_name_link = get_sub_field ( 'location_name_link', 'option' );
    $loc_street = get_sub_field ( 'location_street', 'option' );
    $loc_area = get_sub_field ( 'location_area', 'option' );
    $loc_map = get_sub_field ( 'location_map', 'option' );

    if ( $loc_title ) { echo '<h3 class="block__title">'.$loc_title.'</h3>'; }
    ?>

    <ul class="block__list location">

      <?php
      if ( $loc_name ) { echo '<li class="location__name">'; if ( $loc_name_link ) { echo '<a href="'.$loc_name_link.'" class="location__name__link" rel="nofollow" target="_blank">'.$loc_name.'</a>'; } else { echo $loc_name.'</li>'; } }
      if ( $loc_street ) { echo '<li class="location__street">'.$loc_street.'</li>'; }
      if ( $loc_area ) { echo '<li class="location__area">'.$loc_area.'</li>'; }
      ?>

    </ul>

    <?php
  }
}

<?php
/**
 * Contact social buttons
 *
 * @package hum-v7-core
 */

// enable invididual links
$select_links = get_sub_field( 'contact_links_select' );

// vars
$email = get_field( 'contact_email', 'option' );
$phone = get_field( 'contact_phone', 'option' );
$phone_title = get_field( 'contact_phone_txt', 'option' );
$linkedin = get_field( 'contact_linkedin', 'option' );
$linkedin_title = get_field( 'contact_linkedin_txt', 'option' );
$facebook = get_field( 'contact_facebook', 'option' );
$facebook_title = get_field( 'contact_facebook_txt', 'option' );
$twitter = get_field( 'contact_twitter', 'option' );
$twitter_title = get_field( 'contact_twitter_txt', 'option' );
$instagram = get_field( 'contact_instagram', 'option' );
$instagram_title = get_field( 'contact_instagram_txt', 'option' );


if ( $email || $phone || $linkedin || $facebook || $twitter || $instagram ) {

  ?>
  <div class="button-group button-group--vert">

    <?php
    if ( $email && in_array( 'email', $select_links ) ) {
      echo '<a href="mailto:'.$email.'" class="btn button email" rel="nofollow" target="_blank">'.$email.'</a>';
    }

    if ( $phone && in_array( 'phone', $select_links ) ) {
      echo '<a href="tel:'.$phone.'" class="btn button phone" rel="nofollow" target="_blank">'.$phone_title.'</a>';
    }

    if ( $linkedin && in_array( 'linkedin', $select_links ) ) {
      echo '<a href="'.$linkedin.'" class=" btn button linkedin" rel="nofollow" target="_blank">'.$linkedin_title.'</a>';
    }

    if ( $facebook && in_array( 'facebook', $select_links ) ) {
      echo '<a href="'.$facebook.'" class="btn button facebook" rel="nofollow" target="_blank">'.$facebook_title.'</a>';
    }

    if ( $twitter && in_array( 'twitter', $select_links ) ) {
      echo '<a href="'.$twitter.'" class="btn button twitter" rel="nofollow" target="_blank">'.$twitter_title.'</a>';
    }

    if ( $instagram && in_array( 'instagram', $select_links ) ) {
      echo '<a href="'.$instagram.'" class="btn button instagram" rel="nofollow" target="_blank">'.$instagram_title.'</a>';
    }
    ?>

  </div>
  <?php
}

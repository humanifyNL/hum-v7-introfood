<?php
/**
  * Block page icon
  *
  * ACF field: group_5d0937c170a0c
  *
  * @package hum-v7-core
  */
?>

<article id="page-<?php echo $page->ID; ?>" class="clickable preview preview--page--icon">

  <?php
  // thumbnail
  $img_id = get_field( 'page_icon' , $page->ID );
  if( !empty($img_id) ){

    echo '<div class="block__img">';

      echo wp_get_attachment_image( $img_id, 'medium' );

    echo '</div>';
  }

  // body
  echo '<div class="block__body">';

    echo '<h3 class="block__title">';

      echo '<a class="click block__link" href="'. get_page_link( $page->ID ) .'">';
        echo $page->post_title;
      echo '</a>';

    echo '</h3>';

  echo '</div>';
  ?>

</article><!--.block-->

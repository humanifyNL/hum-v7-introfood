<?php
/**
 * Template part for post-header with slider
 *
 * @package hum-v7-core
 */
?>

<header class="page-header page-header--slider">

  <?php
  if ( have_rows( 'header_img_repeater' ) ) {

    ?>
    <div class="swiper-container">

      <div class="swiper-wrapper">

        <?php
        // image slider
        while ( have_rows( 'header_img_repeater' ) ) {

          the_row();

          $head_img_url = get_sub_field( 'header_img_id' );

          echo '<div class="swiper-slide">';
          echo wp_get_attachment_image( $head_img_url, 'featured', "", array( "class" => "wp-post-image" ) );
          echo '</div>';
        }
        ?>

      </div>

    </div>
    <?php

  } else {

    if ( has_post_thumbnail() ) {
      echo '<div class="page-featured-image">'; the_post_thumbnail( 'featured' ); echo '</div>';
    }
  }

  ?>
  <div class="wrap">

    <?php
    the_title( '<h1 class="page-title">', '</h1>' );

    if ( have_rows( 'page_intro_group' ) ) {
      while ( have_rows( 'page_intro_group' ) ) {

        the_row();
        include( locate_template( 'template-parts/acf/partials/text.php') );
        include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

      }
    }
    ?>

  </div>

</header>

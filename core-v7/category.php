<?php
/**
 * Category template
 *
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive' );
			?>

			<div class="page-content">

				<?php
				if ( have_posts() ) {

					?>
					<section class="row row--previews cats">

						<div class="block-body wrap">

							<div class="grid--previews <?php echo hum_grid_preview();?>">

								<?php
								while ( have_posts() ) {

									the_post();
									include( locate_template( 'template-parts/singles/post/preview-post.php' ) );

								}
								?>

							</div>

						</div>

					</section>
					<?php

				}
				?>

			</div>

		</main>

	</section>

</div>

<?php
get_footer();

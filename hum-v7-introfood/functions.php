<?php
/**
 * hum-base functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package hum-v7-core
 */

if ( ! function_exists( 'hum_core_setup' ) ) {
	/**
	* 1.0 - Theme setup
	*
	* Sets up theme defaults and registers support for various WordPress features.
	*
	* Note that this function is hooked into the after_setup_theme hook, which
	* runs before the init hook. The init hook is too late for some features, such
	* as indicating support for post thumbnails.
	*
	* @uses load_theme_textdomain() For translation/localization support.
	* @uses add_editor_style() To add a Visual Editor stylesheet.
	* @uses add_theme_support() To add support for post thumbnails, automatic feed links, title tag
	* 	custom background, and post formats.
	* @uses register_nav_menu() To add support for navigation menus.
	* @uses set_post_thumbnail_size() To set a custom post thumbnail size.
	*/

	function hum_core_setup() {

		/* Make Hum Base available for translation.
		 *
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/hum-base
		 * If you're building a theme based on Hum Base, use a find and replace
		 * to change 'hum-base' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hum-base' );


		// This theme styles the visual editor with editor.css to match the theme style.
		add_editor_style( array(
			'assets/css/editor.css',
			hum_core_fonts_url(),
		) );


		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );


		// Let WordPress manage the document title tag.
		add_theme_support( 'title-tag' );


		// HTML5 support for default core markup.
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comments__list',
			'gallery',
			'caption',
			'widgets',
		) );


		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(	array(
			'primary' => esc_html__( 'Primary Menu', 'hum-base' ),
			'secondary' => esc_html__( 'Secondary Menu', 'hum-base' ),
			'social'  => esc_html__( 'Social Links Menu', 'hum-base' ),
		) );


		require ( get_template_directory() . '/inc/functions/setup-images.php');


		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );

	}

}

add_action( 'after_setup_theme', 'hum_core_setup' );


/*-----------------------------------------------------------------------------
/* 2.0 Core Functions
*/


// Schema.org rich snippets (microdata), custom BODY classes and ARTICLE (post).
require ( get_template_directory() . '/inc/semantics.php' );

// custom menu class walker
require ( get_template_directory() . '/inc/functions/setup-filters-menu.php' );

// Load fonts
require ( get_template_directory() . '/inc/functions/setup-fonts.php');

// sidebars
require ( get_template_directory() . '/inc/functions/setup-sidebars.php' );

// theme customizer functions
require ( get_template_directory() . '/inc/functions/setup-customizer.php' );

// load the master template tags file
require ( get_template_directory() . '/inc/template-tags.php' );

// Custom post types
require ( get_template_directory() . '/inc/custom-post-types.php');


/*-----------------------------------------------------------------------------
* 3.0 - Enqueue scripts and styles for front-end.
*/

// scripts and styles
require ( get_template_directory() . '/inc/functions/setup-scripts.php');

// admin styles & tweaks
require ( get_template_directory() . '/inc/functions/setup-admin.php');
require ( get_template_directory() . '/inc/functions/setup-admin-columns-posts.php');
require ( get_template_directory() . '/inc/functions/setup-admin-columns-pages.php');

/*-----------------------------------------------------------------------------
/* 4.0 - Optimize performance & security.
*/
require ( get_template_directory() . '/inc/functions/setup-performance.php');

require ( get_template_directory() . '/inc/functions/setup-security.php');

require ( get_template_directory() . '/inc/functions/setup-tweaks.php');


/*-----------------------------------------------------------------------------
/* 5.0 - Query, Yoast, Google & ACF
*/

require ( get_template_directory() . '/inc/functions/setup-google.php');

include ( get_template_directory() . '/inc/functions/setup-queries.php');

include ( get_template_directory() . '/inc/functions/setup-yoast.php');

include ( get_template_directory() . '/inc/acf-options.php');

include ( get_template_directory() . '/inc/functions/setup-acf.php');

// Load ACF custom fields
if ( file_exists( get_template_directory() . '/inc/acf-fields.php' ) ) {
	include ( get_template_directory() . '/inc/acf-fields.php' );
}

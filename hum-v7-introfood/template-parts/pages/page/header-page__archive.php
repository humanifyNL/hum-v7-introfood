<?php
/**
 * Template part for standard post-header
 *
 * @package hum-v7-core
 */
?>

<header class="page-header page-header--archive">

  <?php
  /*
  // featured image
  $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_option('page_for_posts') ),'featured' );
  if ( $img) {

    $featured_image = $img[0];
    echo '<div class="post-featured-image"><img src="'.$featured_image.'" class="wp-post-image"/></div>';
  }
  */

  // breadcrumbs
  get_template_part( 'template-parts/site/yoast', 'breadcrumbs__wrap' );

  // title
  $posts_title = get_field( 'post_archive_title', 'option');

  if ( is_home() && !is_front_page() && $posts_title ) {
    echo '<h1 class="page-title wrap">'.$posts_title.'</h1>';

  } elseif ( is_post_type_archive('products') ) {
    echo '<h1 class="page-title wrap">Product catalog</h1>';

  } else {
    the_archive_title( '<h1 class="page-title wrap">', '</h1>');
  }

  // description
  the_archive_description( '<div class="archive-description wrap">', '</div>' );
  ?>

</header>

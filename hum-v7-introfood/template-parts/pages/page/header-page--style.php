<?php
/**
 * Template part for standard page-header
 *
 * @package hum-v7-core
 */
?>

<header class="page-header <?php echo hum_row_style(true);?>" <?php hum_row_img(true);?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(true); ?>">

      <div class="block block--text block--title">

        <?php
        if ( !is_front_page() ) {
          // breadcrumbs
          get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );
        }
        // title
        the_title( '<h1 class="page-title text-animate">', '</h1>' );

        // text
        if ( have_rows( 'page_intro_group' ) ) {
          while ( have_rows( 'page_intro_group' ) ) {

            the_row();
            include( locate_template( 'template-parts/acf/partials/text.php') );
            include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

          }
        }
        ?>

      </div>

    </div>

  </div>

</header>

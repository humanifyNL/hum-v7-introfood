<?php
/**
 * About content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page--style' );
	?>

	<div class="page-content">

		<?php
		// content
		get_template_part( 'template-parts/acf/flex-page' );
		?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->

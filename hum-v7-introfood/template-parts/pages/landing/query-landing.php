<?php
/**
 * Query pages of custom (landing) page parent
 *
 * ACF field: group_5dd67dff4d49b
 *
 * @package hum-v7-core
 */

$n = 1;
$custom_parent = get_field( 'landing_parent_id', 'option' );
$preview_icon = get_sub_field( 'preview_icon' );

if ( isset($custom_parent) ) {
  $page_parent = $custom_parent;
} else {
  $page_parent = $post->post_parent;
}

$child_pages = get_pages(
  array(
    'child_of'    => $page_parent,
    'sort_column' => 'menu_order',
    'exclude'     => array( get_the_id() ),
  )
);

if ( !empty($child_pages) ) {

  ?>
  <div class="grid--previews <?php if ( $preview_icon ) { echo 'icons '; } echo hum_grid_preview(); ?>">

    <?php
    foreach ($child_pages as $page) {

      setup_postdata( $page );

      if ( $preview_icon ) {

        include( locate_template( 'template-parts/pages/page/preview-page__icon.php' ) );

      } else {

        include( locate_template( 'template-parts/pages/page/preview-page.php' ) );
      }

    }
    wp_reset_postdata();
    ?>

  </div>

  <?php
}

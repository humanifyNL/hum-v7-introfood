<?php
/**
 * Query row - page siblings (related)
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews row--previews--page page_siblings <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

	<div class="wrap">

		<div class="grid <?php echo hum_grid_section(); ?>">

			<?php
			$title_options = get_field( 'page_siblings_title', 'option');
			$title_custom = get_sub_field( 'page_siblings_title_c');
			if ( $title_custom ) {
				$page_sib_title = $title_custom;
			} else {
				$page_sib_title = $title_options;
			}

			if ( $page_sib_title ) {
				echo '<div class="block block--text">';
					echo '<h2>'.$page_sib_title.'</h2>';
				echo '</div>';
			}
			?>

			<div class="block block--previews">

				<?php
				include( locate_template( 'template-parts/pages/page/query-page-siblings.php' ) );
				?>

			</div>

		</div>

	</div>

</section>

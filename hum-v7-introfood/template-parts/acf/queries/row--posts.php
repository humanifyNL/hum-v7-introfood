<?php
/**
 * Query row - posts
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      ?>

      <div class="block block--previews">

        <?php
        include( locate_template( 'template-parts/singles/post/query-posts.php' ) );
        ?>

      </div>

    </div>

  </div>

</section>

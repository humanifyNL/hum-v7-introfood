<?php
/**
 * section with 3 text blocks
 *
 * ACF field: group_60019a19b6ea1
 *
 * @package hum-v7-core
 */
?>

<section class="row row--text row--text-multi <?php echo hum_row_style();?> " <?php hum_row_img(); ?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_preview(); ?>">

      <?php
      $center_align = get_sub_field( 'layout_text_center' );
      if ( have_rows( 'text_repeater' ) ) {
        while ( have_rows( 'text_repeater' ) ) {

          the_row();
          include( locate_template( 'template-parts/acf/blocks/block--textarea.php') );

        }
      }
      ?>

    </div>

  </div>

</section>

<?php
/**
  * Collapsible list row
  *
  * ACF field: group_5f15771f6c489
  *
  * @package hum-v7-core
  */
?>

<section class="row row--collapse <?php echo hum_row_style(); ?>" <?php hum_row_img(); ?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      include( locate_template( 'template-parts/acf/blocks/block--collapse.php') );
      ?>

    </div>

  </div>

</section>

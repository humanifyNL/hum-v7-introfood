<?php
/**
 * Text section with image
 *
 * ACF field: group_5f0b203b4a4af
 *
 * @package hum-v7-core
 */
?>

<section class="row row--gallery <?php echo hum_row_style();?>" <?php hum_row_img(); ?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      include( locate_template( 'template-parts/acf/blocks/block--gallery.php') );
      ?>

    </div>

  </div>

</section>

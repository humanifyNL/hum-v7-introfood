<?php
/**
 * Block--textarea
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'text' ) ) {

  ?>
  <div class="block block--text block--textarea <?php if ( $center_align ) { echo ' center'; } echo hum_block_style();?>">

    <?php
    include( locate_template( 'template-parts/acf/partials/image__icon.php') );
    include( locate_template( 'template-parts/acf/partials/title.php') );
    include( locate_template( 'template-parts/acf/partials/text.php') );
    ?>

  </div>
  <?php

}

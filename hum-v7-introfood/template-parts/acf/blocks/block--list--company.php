<?php
/**
 * Block--company
 *
 * @package hum-v7-core
 */

$enable_address = get_sub_field( 'enable_contact_address' );
$enable_company = get_sub_field( 'enable_contact_company' );

if ( $enable_company || $enable_address ) {

  ?>
  <div class="block block--list block--list--company">

    <?php
    if ( $enable_address ) {
      include( locate_template( 'template-parts/acf/partials/address.php') );
    }
    if ( $enable_company ) {
      include( locate_template( 'template-parts/acf/partials/company.php') );
    }
    ?>

  </div>
  <?php

}

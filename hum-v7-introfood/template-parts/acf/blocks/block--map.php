<?php
/**
 * Block--map
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'map' ) ) {

  ?>
  <div class="block block--map">

    <?php
    include( locate_template( 'template-parts/acf/partials/map.php' ) );
    ?>

  </div>
  <?php
  
}

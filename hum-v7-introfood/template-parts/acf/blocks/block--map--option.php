<?php
/**
 * Block--map-opt
 *
 * @package hum-v7-core
 */

if ( have_rows( 'contact_location_rep', 'option' ) ) {

  ?>
  <div class="block block--map">

    <?php
    while ( have_rows( 'contact_location_rep', 'option' ) ) {

      echo '<div class="block__map">';

        the_row();
        $map_loc = get_sub_field( 'location_map', 'option' );
        $map_loc_name = get_sub_field ( 'location_name', 'option' );

        echo '<div class="marker" data-lat="'.$map_loc['lat'].'" data-lng="'.$map_loc['lng'].'">';
          if ( $map_loc_name ) { echo $map_loc_name; }
        echo '</div>';

      echo '</div>';
    }
    ?>

  </div>
  <?php

}

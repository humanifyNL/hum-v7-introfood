<?php
/**
 * Block--slider
 *
 * @package hum-v7-core
 */

if ( have_rows( 'swiper_slides' ) ) {

  ?>
  <div class="block block--slider">

    <?php
    include( locate_template( 'template-parts/acf/partials/slider.php') );
    ?>

  </div>
  <?php
  
}

<?php
/**
 * Page content WP tinyMCE
 *
 * @package hum-v7-core
 */

if ( !empty( get_the_content() ) ) {

  ?>
  <section class="row row--text">

    <div class="wrap">

      <div class="grid">

        <div class="block block--text">

          <div class="block__text">

            <?php
            the_content();
            ?>

          </div>

        </div>

      </div>

    </div>

  </section>
  <?php
}

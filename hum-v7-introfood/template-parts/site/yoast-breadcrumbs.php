<?php
/**
 * Yoast breadcrumbs code
 *
 * @package hum-v7-core
 */

if ( function_exists('yoast_breadcrumb') ) {

  echo '<div class="yoast">';

  yoast_breadcrumb( '
  <p class="breadcrumbs">','</p>
  ' );

  echo '</div>';
}

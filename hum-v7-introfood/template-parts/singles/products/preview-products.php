<?php
/**
 * Post preview part
 *
 * @package hum-v7-core
 */

?>
<article id="post-<?php the_id();?>" class="clickable preview preview--post preview--product <?php if ( !empty($enable_iso) ) { hum_iso_class('category'); }?>">

  <?php
  echo '<h3 class="block__title">'; the_title(); echo '</h3>';

  // thumbnail
  $thumb = get_the_post_thumbnail( $post->ID,'medium' );

  echo '<div class="block__thumb">';
    if ( !$thumb ) { echo hum_default_img();
    } else { echo $thumb; }
  echo '</div>';
  ?>

  <div class="block__body">

    <?php
    //hum_excerpt( 'block__text is-excerpt' );

    // link
    $link_title = get_field( 'post_links_title' , 'option');
    echo '<div class="block__footer">';
      echo '<a href="'; the_permalink(); echo '" class="'. hum_button_class( 'post' ). '">'.$link_title.'</a>';
    echo '</div>';
    ?>

  </div>

</article>

<?php
/**
 * Post header
 *
 * @package hum-v7-core
 */
?>

<header class="post-header">

  <div class="wrap wrap--post">

    <div class="grid grid--60">

      <div class="block block--image">

        <?php
        // post image
        if ( has_post_thumbnail() ) {
          echo '<div class="post-featured-image">'; the_post_thumbnail( 'featured-sq' ); echo '</div>';
        }
        ?>

      </div>

      <div class="block block--meta">

        <?php
        // breadcrumbs
        get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );

        // title
        the_title( '<h1 class="post-title wrap">', '</h1>' );

        get_template_part( 'template-parts/singles/post/meta', 'post__top' );

        if ( have_rows( 'product_info_grp' ) ) {
          while ( have_rows( 'product_info_grp' ) ) {
            the_row();
            get_template_part( 'template-parts/singles/products/product-info' );
          }
        }
        ?>

      </div>


    </div>

  </div>

</header>

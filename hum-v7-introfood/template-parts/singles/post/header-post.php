<?php
/**
 * Post header
 *
 * @package hum-v7-core
 */
?>

<header class="post-header">

  <div class="block-body wrap wrap--post">

    <div class="grid">

      <div class="block block--meta">

        <?php
        // breadcrumbs
        get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );

        // title
        the_title( '<h1 class="post-title wrap">', '</h1>' );

        get_template_part( 'template-parts/singles/post/meta', 'post__top' );
        ?>

      </div>

      <div class="block block--image">

        <?php
        // post image
        if ( has_post_thumbnail() ) {
          echo '<div class="post-featured-image">'; the_post_thumbnail( 'featured-sq' ); echo '</div>';
        }
        ?>

      </div>

    </div>

  </div>

</header>

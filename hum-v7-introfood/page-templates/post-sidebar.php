<?php
/**
 * Template Name: Sidebar
 * Template Post Type: post
 *
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main wrap-main--sb">

  <div id="primary" class="content-area">

  	<main id="main" class="site-main">

  		<?php
  		while ( have_posts() ) {

        the_post();
        get_template_part( 'template-parts/singles/content', get_post_type() );

        /*
        if ( comments_open() || get_comments_number() ) {
          comments_template();
        }
        */
      }
  		?>

  	</main>

  </div>

  <?php
  get_sidebar();
  ?>

</div>

<?php
get_footer();

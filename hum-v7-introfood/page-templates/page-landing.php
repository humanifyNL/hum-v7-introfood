<?php
/**
 * Template Name: Landing
 * Template Post Type: page
 *
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

  <div id="primary" class="content-area">

  	<main id="main" class="site-main">

  		<?php
  		while ( have_posts() ) {

        the_post();
        get_template_part( 'template-parts/pages/content', 'landing' );

      }
  		?>

  	</main>

  </div>

</div>

<?php
get_footer();

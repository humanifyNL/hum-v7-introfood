<?php
/**
 * Template Name: Page group
 * Template Post Type: page
 *
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) {

	      the_post();
	      get_template_part( 'template-parts/pages/content', 'page-group' );

	    }
	    ?>

  	</main>

	</div>

</div>

<?php
get_footer();

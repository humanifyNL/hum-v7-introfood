/**
 * Add click class for clickable.scss
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

	$('.clickable').on('click', function () {
		window.location.href = $(this).find('.click').attr('href');
	});

});

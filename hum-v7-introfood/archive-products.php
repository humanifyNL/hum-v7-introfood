<?php
/**
 * The template for displaying archive page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<section id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			get_template_part( 'template-parts/pages/page/header', 'page__archive' );
			?>

			<div class="page-content">

				<?php
	      if ( have_posts() ) {

					?>
					<section class="row row--previews">

						<div class="block-body wrap">

							<div class="grid--previews <?php echo hum_grid_preview();?>">

								<?php
								while ( have_posts() ) {

									the_post();

									include( locate_template( 'template-parts/singles/products/preview-products.php' ) );

								}
								?>

							</div>

						</div>

					</section>

					<?php
					hum_archive_page_nav();

	      }
				?>

			</div>

		</main>

	</section>

</div>

<?php
get_footer();

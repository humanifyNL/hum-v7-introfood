<?php
/**
 * Hum core scripts
 *
 * @package hum-v7-core
 */

function hum_core_scripts_styles() {

	global $wp_styles;

	/* Load font stylesheet URL for Open Sans and other optional Google fonts. Open Sans is default Hum Base font.
	 * Google recommends to load this stylesheet before any other stylesheet.
	 * You can comment out this section if you want to remove Open Sans as default font.
	*/
	wp_enqueue_style( 'hum_fonts',
	hum_core_fonts_url(),
	array(),
	null );

	/* Add CSS file of the Font Awesome icon font (local version), used in the main stylesheet.*/
	wp_enqueue_style( 'font-awesome',
	get_template_directory_uri() . '/assets/fonts/font-awesome/css/font-awesome.min.css',
	array(),
	'4.7.0',
	'all' );


	/* Add CSS file of the Font Awesome icon font, used in the main stylesheet - BootstrapCDN version.
	 *
	 * To load Font Awesome icon font from BootstrapCDN, replace the line in the section above:
	 * get_template_directory_uri() . '/assets/fonts/font-awesome/css/fontawesome-all.min.css',
	 * with:
	 * '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/fontawesome-all.min.css',
	 * Please be advised, that overall loading CSS is recommended from your own domain - it is faster, because DNS will be resolved once.
	 */

	 /* Add JavaScript for handling the navigation menu hide-and-show behavior.*/
	 wp_enqueue_script( 'hum_navigation',
	 get_template_directory_uri() . '/assets/js/navigation.js',
	 array(),
	 '6.0.0',
	 true ); // Loading script in the footer for a better performance


	/* Main stylesheet.*/
	wp_enqueue_style( 'hum_core-style',
	get_stylesheet_uri(),
	array(),
	'6.0.0' );


	/* Load html5 shiv to add support for HTML5 elements in older IE versions.*/
	wp_enqueue_script( 'hum_html5',
	get_template_directory_uri() . '/assets/js/html5shiv.min.js',
	array(),
	'3.7.3' );
	wp_script_add_data( 'hum_html5', 'conditional', 'lt IE 9' );


	/* Add JS to pages with the comment form for threaded comments (when in use) */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/* Add swiper css */
	wp_enqueue_style( 'hum_base-swiper',
	get_template_directory_uri() . '/assets/css/swiper.min.css',
	array(),
	'5.4.5' );

  /* remove the block library for wp gutenberg */
	wp_dequeue_style( 'wp-block-library' );

	/* load JS scripts for functionality */
	include ( get_template_directory() . '/inc/functions/setup-scripts-js.php');


}
add_action( 'wp_enqueue_scripts', 'hum_core_scripts_styles' );

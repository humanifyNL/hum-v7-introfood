<?php
/**
 * Hum Base buttons for isotope
 * $query -> insert valid wp_query - default: $wp_query
 * $taxonomy -> button taxonomy sorting - default: category
 *
 * @package hum-v7-core
 */


if ( !function_exists( 'hum_get_query_buttons_iso' ) ) {

  function hum_get_query_buttons_iso( $query, $taxonomy ) {

    // text input
    $button_all = 'X';

    // get term objects
    $post_terms = get_terms( array(
      'taxonomy' => $taxonomy,
      'hide_empty' => true,
      'include' => hum_term_extract( $query, 'post_tag' ),
    ) );

    if ( $post_terms ) {

      // build html
      echo '<div class="button-group button-group--iso">';

      echo '<button class="btn button--alt is-checked" data-filter="*">'.$button_all.'</button>';


      foreach ( $post_terms as $post_term ) {

        $term_id = $post_term->term_id;
        $term_slug = $post_term->slug;
        $term_name = $post_term->name;

        // skip uncategorized
        if ( $term_id == 1 ) {
          continue;
        }

        echo '<button class="btn button--alt" data-filter=".'. $term_slug .'">'. $term_name .'</button>';
      }

      echo '</div>';
    }
  }
}

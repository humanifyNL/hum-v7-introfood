<?php
/**
 * Grid section class modifier
 *
 * ACF field: group_5f0990f32e941
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_grid_section' ) ) {

  function hum_grid_section( $head = false ) {

    $grid_selected = get_sub_field( 'section_grid_select' );
    $grid_reverse = get_sub_field( 'section_grid_reverse' );

    if ( $head && have_rows( 'page_intro_group' ) ) {
      while ( have_rows( 'page_intro_group' ) ) {

        the_row();
        $grid_selected = get_sub_field( 'section_grid_select' );
        $grid_reverse = get_sub_field( 'section_grid_reverse' );

      }
    }

    if ( !empty($grid_selected) ) {

      switch ( $grid_selected  ) {

        case 'grid-60':
          $grid = 'grid--60';
          break;
        case 'grid-50':
          $grid = 'grid--50';
          break;
        case 'grid-40':
          $grid = 'grid--40';
          break;
        case 'grid-100':
          $grid = 'grid--100';
          break;

      }

      if ( $grid_reverse  ) {

        return $grid . ' reverse';

      } else {

        return $grid;
      }

    // if no grid selected
    } else {

      if ( $grid_reverse ) {

        return 'reverse';

      } else {

        return;
      }
    }
  }
}

/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = section_grid_select
 */

function acf_load_grid_section_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'grid-60' => '60% - 40%',
      'grid-50' => '50% - 50%',
      'grid-40' => '40% - 60%',
      'grid-100' => '100%',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=section_grid_select', 'acf_load_grid_section_field_choices');

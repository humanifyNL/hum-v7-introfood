<?php
/**
 * List style modifier for text-R partial
 *
 * ACF field: group_601d6dc16c24d
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_list_style' ) ) {

  function hum_list_style() {

    $list_select = get_sub_field( 'list_style' );

    if ( isset($list_select) ) {

      if ( $list_select == 'theme' ) {

        $theme = 'list_block';

      } elseif ( $list_select == 'theme-alt' ) {

        $theme = 'list_fields';

      } elseif ( $list_select == 'default' ) {

        return;

      } else {

        return;
      }

      return $theme;
    }
  }
}



/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = list_style
 */

function acf_load_list_style_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'default' => 'Default',
      'theme' => 'Thema blok',
      'theme-alt' => 'Thema vlakken',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=list_style', 'acf_load_list_style_field_choices');

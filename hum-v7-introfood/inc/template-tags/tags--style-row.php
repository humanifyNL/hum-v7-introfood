<?php
/**
 * Row theme layout
 *
 * ACF field: group_5f76e008a957f
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_row_style' ) ) {

  function hum_row_style( $head = false) {

    $theme_select = get_sub_field( 'row_theme' );
    $theme_compact = get_sub_field( 'row_compact' );
    $theme_space = get_sub_field( 'row_more_space' );

    if ( $head && have_rows( 'page_intro_group' ) ) {
      while ( have_rows( 'page_intro_group' ) ) {

        the_row();
        $theme_select = get_sub_field( 'row_theme' );
        $theme_compact = get_sub_field( 'row_compact' );
        $theme_space = get_sub_field( 'row_more_space' );
      }
    }

    if ( isset($theme_select) ) {

      if ( $theme_select !== 'default' ) {

        $theme = $theme_select;

        if ( $theme_space && !$theme_compact ) {
          $theme .= ' is-spaced';
        } elseif ( $theme_space && $theme_compact) {
          $theme .= ' is-spaced';
        } elseif ( !$theme_space && $theme_compact ) {
          $theme .= ' is-compact';
        } elseif ( $theme_compact ) {
          $theme .= ' is-compact';
        }


      } else {

        if ( $theme_space && !$theme_compact ) {
          $theme .= ' is-spaced';
        } elseif ( $theme_space && $theme_compact) {
          $theme .= ' is-spaced';
        } elseif ( !$theme_space && $theme_compact ) {
          $theme .= ' is-compact';
        } elseif ( $theme_compact ) {
          $theme .= ' is-compact';
        } else {
          return;
        }

      }

      return $theme;

    }
  }
}

/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = row_theme
 */

function acf_load_row_theme_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'default' => 'Main theme',
      'has-bg' => 'Background',
      'style-1' => 'Primary style',
      'has-img' => 'Image background',
      'has-img parallax' => 'Image parallax',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=row_theme', 'acf_load_row_theme_field_choices');


/* Add background img
 *
 */

if ( !function_exists( 'hum_row_img' ) ) {

  function hum_row_img( $head = false) {

    $theme_select = get_sub_field( 'row_theme' );
    // site options
    $img_url = get_field( 'hum_bg_url', 'option' );
    // set up in the row
    $img_url_row = get_sub_field( 'bg_img_url_row' );

    if ( $head && have_rows( 'page_intro_group' ) ) {
      while ( have_rows( 'page_intro_group' ) ) {
        the_row();
        $theme_select = get_sub_field( 'row_theme' );
        $img_url_row = get_sub_field( 'bg_img_url_row' );
      }
    }

    if ( $img_url_row ) {
      $bg_img_url = $img_url_row;
    } else {
      $bg_img_url = $img_url;
    }

    // custom bg
    if ( $bg_img_url && $theme_select == 'has-img' || $theme_select == 'has-img parallax' ) {

      $string = 'style="background-image: url('.$bg_img_url.')"';

    } else {

      return;

    }

    echo $string;
  }
 }

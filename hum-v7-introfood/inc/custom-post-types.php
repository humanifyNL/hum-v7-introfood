<?php
/**
 * Custom post functions
 *
 * @package hum-v7-core
 */

/**
 * Custom Post Types
 *
 */

function create_post_type() {

  register_post_type( 'products',
    array(
      'labels' => array(
        'name'              => __( 'Products' ),
        'singular_name'     => __( 'Product' ),
  			'all_items'         => __( 'All Products' ),
  			'view_item'         => __( 'View Product' ),
  			'add_new_item'      => __( 'New Product' ),
  			'add_new'           => __( 'New Product' ),
  			'edit_item'         => __( 'Edit Product' ),
  			'update_item'       => __( 'Update Product' ),
  			'search_item'       => __( 'Search Product' ),
      ),
      'public'              => true,
      'has_archive'   	    => true,
			'hierarchical'        => false,
			'publicly_queryable'  => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
	 	 	'show_in_nav_menus'   => true,
	 		'show_in_admin_bar'   => true,
      'menu_icon'           => 'dashicons-analytics',
			'supports'            => array( 'title','thumbnail','editor','hum_base_postexcerpt' ),
			'taxonomies'          => array( 'post_tag','category' ),
			'rewrite'             => array( 'slug' => 'products' ),
		)
  );

  /*
  register_post_type( 'clients',
    array(
      'labels' => array(
        'name'              => __( 'Clients' ),
        'singular_name'     => __( 'Client' ),
        'all_items'         => __( 'All Clients' ),
        'view_item'         => __( 'View Client' ),
        'add_new_item'      => __( 'New Client' ),
        'add_new'           => __( 'New Client' ),
        'edit_item'         => __( 'Edit Client' ),
        'update_item'       => __( 'Update Client' ),
        'search_item'       => __( 'Search Client' ),
      ),
      'public'              => true,
      'has_archive'   	    => true,
      'hierarchical'        => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'menu_icon'           => 'dashicons-groups',
      'supports'            => array('title','thumbnail','editor','hum_base_postexcerpt'),
      //'taxonomies'        => array( 'post_tag' ),
      'rewrite'             => array('slug' => 'clients'),
    )
  );
  */

}
add_action( 'init', 'create_post_type' );



/**
 * Custom taxonomies
 *
 */
/*
// Add new taxonomy, NOT hierarchical (like tags)
$labels = array(
	'name'                       => _x( 'Tags', 'taxonomy general name'),
	'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
	'search_items'               => __( 'Search Tags' ),
	'popular_items'              => __( 'Popular Tags' ),
	'all_items'                  => __( 'All Tags' ),
	'parent_item'                => null,
	'parent_item_colon'          => null,
	'edit_item'                  => __( 'Edit Tag' ),
	'update_item'                => __( 'Update Tag' ),
	'add_new_item'               => __( 'Add New Tag' ),
	'new_item_name'              => __( 'New Tag Name' ),
	'separate_items_with_commas' => __( 'Separate Tags with commas' ),
	'add_or_remove_items'        => __( 'Add or remove Tags' ),
	'choose_from_most_used'      => __( 'Choose from the most used Tags'),
	'not_found'                  => __( 'No Tags found.' ),
	'menu_name'                  => __( 'Tags' ),
);

$args = array(
	'hierarchical'          => false,
	'labels'                => $labels,
	'show_ui'               => true,
	'show_admin_column'     => true,
	'update_count_callback' => '_update_post_term_count',
	'query_var'             => true,
	'rewrite'               => array( 'slug' => 'tag' ),
);

register_taxonomy( 'tag-web', array('post'), $args );
*/



/**
 * Add custom post to Cat archives.
 *
 */
/*
function hum_category_set_post_types( $query ){

  if( $query->is_category() && $query->is_main_query() ){
    $query->set( 'post_type', array( 'post_web' ) );
  }
}
add_action( 'pre_get_posts', 'hum_category_set_post_types' );
*/
